function callAjax() {
    var root = 'http://jsonplaceholder.typicode.com';

    $.ajax({
        url: root + '/Users',
        method: 'GET'
    }).then(function (data) {
        var tableUsers;

        for (var p in data) {
            tableUsers += "<tr>"
            tableUsers += "<td>" + data[p].id + "</td>";
            tableUsers += "<td>" + data[p].name + "</td>";
            tableUsers += "<td>" + data[p].username + "</td>";
            tableUsers += "<td>" + data[p].email + "</td>";
            tableUsers += "<td>" + data[p].address.street + "</td>";
            tableUsers += "<td>" + data[p].address.suite + "</td>";
            tableUsers += "<td>" + data[p].address.city + "</td>";
            tableUsers += "<td>" + data[p].address.zipcode + "</td>";
            tableUsers += "<td>" + "</td>";
            tableUsers += "<td>" + data[p].address.geo.lat + "</td>";
            tableUsers += "<td>" + data[p].address.geo.lng + "</td>";
            tableUsers += "<td>" + data[p].phone + "</td>";
            tableUsers += "<td>" + data[p].website + "</td>";
            tableUsers += "<td>" + data[p].company.name + "</td>";
            tableUsers += "<td>" + data[p].company.catchPhrase + "</td>";
            tableUsers += "<td>" + data[p].company.bs + "</td>";
        }
        $("#Users").append(tableUsers)
    });
}







