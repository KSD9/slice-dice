function currentWeather() {
    switch (parseInt($('#option option:selected').val())) {
        case 1:
            plovdivWeather();
            break;
        case 2:
            capeTownWeather();
    }
}

function plovdivWeather() {
    var root = 'http://api.openweathermap.org/data/2.5/weather?q=Plovdiv,bg&APPID=6b889128b213715c40c100b001ec2a21';

    $.ajax({
        url: root,
        method: 'GET'
    }).then(function (data) {
        console.log(data);
        var weather;

        $("#cityName").append(":" + data.name);

        if (data.weather["0"].main == "Rain") {
            $("#weatherImg").append('<img src="./img/cloudy.png" height="20px" width="20px">');
        }
        else if (data.weather["0"].main == "Clear") {
            $("#weatherImg").append('<img src="./img/sunny.png" height="20px" width="20px">');
        }
        $("#currentTemp").append(data.main.temp);
        $("#minTemp").append(":" + data.main.temp_min);
        $("#maxTemp").append(":" + data.main.temp_max);
        $("#wind").append(":" + data.wind.speed);
    }
        );
}


function capeTownWeather() {
    var root = 'http://api.openweathermap.org/data/2.5/weather?id=3369157&APPID=6b889128b213715c40c100b001ec2a21'

    $.ajax({
        url: root,
        method: 'GET'
    }).then(function (data) {
        console.log(data);
        var weather;

        $("#cityName").replaceWith('<span>City Name:</span>' + data.name);


        if (data.weather["0"].main == "Rain") {
            $("#weatherImg").replaceWith('<img src="./img/cloudy.png" height="20px" width="20px">');
        }
        else if (data.weather["0"].main == "Clear") {
            $("#weatherImg").replaceWith('<img src="./img/sunny.png" height="20px" width="20px">');
        }
        $("#currentTemp").replaceWith('<span>Current Temp:</span>' + data.main.temp);
        $("#minTemp").replaceWith('<span>Min Temp:</span>' + data.main.temp_min);
        $("#maxTemp").replaceWith('<span>Max temp:</span>' + data.main.temp_max);
        $("#wind").replaceWith('<span>Wind:</span>' + data.wind.speed);
    }
        );
}