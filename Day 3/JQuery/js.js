function hideImg() {
    $("#carImg").css("display", "none");

}

function showImg() {
    $("#carImg").css("display", "block");

}
function roundCorner() {
    $("#carImg").css("border-radius", "20%");

}

function straightCorners() {
    $("#carImg").css("border-radius", "0%");

}

function addRedBorder() {
    $("#carImg").css("border", "3px solid red");

}

function removeRedBorder() {
    $("#carImg").css("border", "0px");

}
function moveUp() {

    $("#carImg").animate({ "top": "-=10px" }, "slow");

}

function moveDown() {
    $("#carImg").animate({ "top": "+=10px" }, "slow");

}

function nextImage() {
    var src = ($("#carImg").attr('src') === './img/cls.jpg')
        ? './img/martin.jpg'
        : './img/cls.jpg';
    $("#carImg").attr('src', src);

}
function previousImage() {
    var src = ($("#carImg").attr('src') === './img/martin.jpg')
        ? './img/cls.jpg'
        : './img/martin.jpg';
    $("#carImg").attr('src', src);

}



