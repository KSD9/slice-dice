$(document).ready(function () {
    var Table = $('#Users').DataTable({
        "columns": [

            { "data": "id" },
            { "data": "name" },
            { "data": "username" },
            { "data": "email" },
            { "data": "address.street" },
            { "data": "address.suite" },
            { "data": "address.city" },
            { "data": "address.zipcode" },
            { "data": "address.geo.lat" },
            { "data": "address.geo.lng" },
            { "data": "phone" },
            { "data": "website" },
            { "data": "company.name" },
            { "data": "company.catchPhrase" },
            { "data": "company.bs" },
        ]

    });

    $('#FetchData').on("click", function () {
        $.ajax({
            url: "http://jsonplaceholder.typicode.com/users",
            "dataSrc": ""
        }).done(function (result) {
            Table.clear().draw();
            Table.rows.add(result).draw();
        }).fail(function (jqXHR, textStatus, errorThrown) {

        });
    }
    )
});








