$(document).ready(function () {
    var Table = $('#Users').DataTable({
        "columns": [

            { "data": "id" },
            { "data": "name" },
            { "data": "username" },
            { "data": "email" },
            { "data": "address.street" },
            { "data": "address.suite" },
            { "data": "address.city" },
            { "data": "address.zipcode" },
            { "data": "address.geo.lat" },
            { "data": "address.geo.lng" },
            { "data": "phone" },
            { "data": "website" },
            { "data": "company.name" },
            { "data": "company.catchPhrase" },
            { "data": "company.bs" },
        ]

    });

    $('#FetchData').on("click", function () {
        $.ajax({
            url: "http://jsonplaceholder.typicode.com/users",
            "dataSrc": ""
        }).done(function (result) {
            Table.clear().draw();
            Table.rows.add(result).draw();
        }).fail(function (jqXHR, textStatus, errorThrown) {

        });
    }
    )
});


function Validate() {
    $.validator.setDefaults({
        submitHandler: function () {
            $.ajax({
                method: "POST",
                url: "http://jsonplaceholder.typicode.com/users",
                data: $("#UserForm").serialize(),
                success: Sucess,
                error: NotSucess
            });
        }
    });
    $("#UserForm").validate();
}

function Sucess() {
    new PNotify({
        title: 'Ok',
        text: 'Success',
        type: 'success'
    });
    $.clearInput = function () {
        $("#UserForm").find('input[type=text],  input[type=email], [type=url], textarea').val('');
    };
    $.clearInput();
}

function NotSucess() {
    new PNotify({
        title: 'Sorry',
        text: 'Something went wrong',
        type: "error"
    });
}



